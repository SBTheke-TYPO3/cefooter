<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace SBTheke\Cefooter\Preview;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Backend\View\BackendLayout\Grid\GridColumnItem;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\LinkHandling\Exception\UnknownLinkHandlerException;
use TYPO3\CMS\Core\LinkHandling\LinkService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class StandardContentPreviewRenderer extends \TYPO3\CMS\Backend\Preview\StandardContentPreviewRenderer
{
    /**
     * @throws UnknownLinkHandlerException
     */
    public function renderPageModulePreviewFooter(GridColumnItem $item): string
    {
        $record = $item->getRecord();
        $pageTSconfig = BackendUtility::getPagesTSconfig($record['pid']);

        $recordNotVisible = $record['hidden']
            || ($record['starttime'] && $record['starttime'] > time())
            || ($record['endtime'] && $record['endtime'] < time());
        if (!empty($pageTSconfig['mod.']['web_layout.']['tt_content_footer.']['hideIfNotVisible']) && $recordNotVisible) {
            return '';
        }
        $showFields = GeneralUtility::trimExplode(',', @$pageTSconfig['mod.']['web_layout.']['tt_content_footer.']['show'], true);
        foreach($showFields as $k => $field) {
            // Check TCA
            if (empty($GLOBALS['TCA']['tt_content']['columns'][$field])) {
                unset($showFields[$k]);
                continue;
            }

            // Check conditions
            $showFieldCond = @$pageTSconfig['mod.']['web_layout.']['tt_content_footer.']['displayCond.'][$field . '.'];
            if(is_array($showFieldCond)) {
                foreach($showFieldCond as $cond) {
                    $condField = $field;
                    if(!empty($cond['hideIfNotVisible']) && $recordNotVisible) {
                        unset($showFields[$k]);
                    }
                    if(!empty($cond['field'])) {
                        $condField = $cond['field'];
                    }
                    if(!empty($cond['valuesNot'])) {
                        if(in_array(
                            $record[$condField],
                            GeneralUtility::trimExplode(',', $cond['valuesNot'])
                        )) {
                            unset($showFields[$k]);
                        }
                    }
                    if(!empty($cond['values'])) {
                        if (is_int($record[$condField])) {
                            $valuesArray = GeneralUtility::intExplode(',', $cond['values']);
                        } else {
                            $valuesArray = GeneralUtility::trimExplode(',', $cond['values']);
                        }
                        if(!in_array(
                            $record[$condField],
                            $valuesArray
                        )) {
                            unset($showFields[$k]);
                        }
                    }
                }
            }
        }
        if($showFields) {
            $info = [];
            $this->getProcessedValue($item, implode(',', $showFields), $info);

            if (!empty($info)) {
                return implode('<br>', $info);
            }
        }
        return '';
    }

    /**
     * @throws UnknownLinkHandlerException
     */
    protected function getProcessedValue(GridColumnItem $item, string $fieldList, array &$info): void
    {
        $itemLabels = $item->getContext()->getItemLabels();
        $record = $item->getRecord();

        $pageTSconfig = BackendUtility::getPagesTSconfig($record['pid']);
        $showSlugFor = GeneralUtility::trimExplode(',', @$pageTSconfig['mod.']['web_layout.']['tt_content_footer.']['showSlugFor'], true);
        $linkService = GeneralUtility::makeInstance(LinkService::class);
        $pageRepository = GeneralUtility::makeInstance(PageRepository::class);

        $fieldArr = explode(',', $fieldList);
        foreach ($fieldArr as $field) {
            if (!empty($record[$field])) {
                $fieldValue = BackendUtility::getProcessedValue('tt_content', $field, $record[$field], 0, false, false, $record['uid'] ?? 0) ?? '';
                if (in_array($field, $showSlugFor)) {
                    $linkData = $linkService->resolve($fieldValue);
                    if (!empty($linkData['pageuid'])) {
                        $pageData = $pageRepository->getPage_noCheck($linkData['pageuid']);
                        $fieldValue = $pageData['slug'] ?: $fieldValue;
                    }
                }
                $info[] = '<strong>' . htmlspecialchars((string)($itemLabels[$field] ?? '')) . '</strong> ' . htmlspecialchars($fieldValue);
            }
        }
    }
}
