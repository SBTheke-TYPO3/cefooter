<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace SBTheke\Cefooter\Preview;

use TYPO3\CMS\Backend\View\BackendLayout\Grid\GridColumnItem;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FormPagePreviewRenderer extends \TYPO3\CMS\Form\Hooks\FormPagePreviewRenderer
{
    public function renderPageModulePreviewFooter(GridColumnItem $item): string
    {
        $standardContentPreviewRenderer = GeneralUtility::makeInstance(StandardContentPreviewRenderer::class);
        return $standardContentPreviewRenderer->renderPageModulePreviewFooter($item);
    }
}
