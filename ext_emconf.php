<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "cefooter".
 *
 * Auto generated 30-12-2023 18:52
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Content element footer info',
  'description' => 'Preview of settings in footer of tt_content element in page module.',
  'category' => 'be',
  'version' => '2.1.2',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => true,
  'author' => 'Sven Burkert',
  'author_email' => 'bedienung@sbtheke.de',
  'author_company' => 'SBTheke web development',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '12.4.0-12.4.99',
    ),
    'suggests' => 
    array (
      'news' => 'Just for loading order',
      'rte_ckeditor_image ' => 'Just for loading order',
    ),
    'conflicts' => 
    array (
    ),
  ),
  'autoload' => 
  array (
  ),
);

