<?php
defined('TYPO3') || die();

(function($table) {
    // Register preview renderer
    $GLOBALS['TCA'][$table]['ctrl']['previewRenderer'] =
        \SBTheke\Cefooter\Preview\StandardContentPreviewRenderer::class;
    $GLOBALS['TCA'][$table]['types']['text']['previewRenderer'] =
        \SBTheke\Cefooter\Preview\TextPreviewRenderer::class;
    $GLOBALS['TCA'][$table]['types']['image']['previewRenderer'] =
        \SBTheke\Cefooter\Preview\ImagePreviewRenderer::class;
    $GLOBALS['TCA'][$table]['types']['textmedia']['previewRenderer'] =
        \SBTheke\Cefooter\Preview\TextmediaPreviewRenderer::class;
    $GLOBALS['TCA'][$table]['types']['textpic']['previewRenderer'] =
        \SBTheke\Cefooter\Preview\TextpicPreviewRenderer::class;
    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('form')) {
        $GLOBALS['TCA'][$table]['types']['form_formframework']['previewRenderer'] =
            \SBTheke\Cefooter\Preview\FormPagePreviewRenderer::class;
    }
    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('news')) {
        $GLOBALS['TCA'][$table]['types']['news_pi1']['previewRenderer'] =
            \SBTheke\Cefooter\Preview\NewsPluginPreviewRenderer::class;
        $GLOBALS['TCA'][$table]['types']['news_taglist']['previewRenderer'] =
            \SBTheke\Cefooter\Preview\NewsPluginPreviewRenderer::class;
    }

    // Overwrite labels
    $GLOBALS['TCA'][$table]['columns']['header_layout']['label'] = 'LLL:EXT:cefooter/Resources/Private/Language/locallang.xlf:header_layout';
    $GLOBALS['TCA'][$table]['columns']['header_position']['label'] = 'LLL:EXT:cefooter/Resources/Private/Language/locallang.xlf:header_position';
})('tt_content');
