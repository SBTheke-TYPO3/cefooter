<?php
defined('TYPO3') || die('Access denied.');

// Register Page TSconfig
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'cefooter',
    'Configuration/TsConfig/mod.tsconfig',
    'EXT:cefooter: Preview settings'
);
