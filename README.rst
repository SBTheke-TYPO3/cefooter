=============
Documentation
=============

----------------
What does it do?
----------------

This extension shows information of a content element in the footer of the content element in TYPO3 BE in the page module:

.. figure:: Documentation/screenshot1.png


It's configurable with Page TSconfig, which information are shown and under what conditions, e.g. the field "layout" is only shown when layout is not "none" or "default", or field "subheader" is only shown when the content type is "textpic".


-------------
Configuration
-------------


Add Page TSconfig
=================

Add the predefined Page TSconfig by selecting item "EXT:cefooter: Preview settings" in your ROOT page or write your own Page TSconfig.


Configuration
=============

Configure the fields which should be shown in the content element footer:

::

    mod.web_layout.tt_content_footer.show = frame_class, layout, subheader


Do not show any field in the content element footer:

::

    mod.web_layout.tt_content_footer.show =


Do not show any field in the content element footer, if content element is not visible:

::

    mod.web_layout.tt_content_footer.hideIfNotVisible = 1


Show slug (speaking url) instead of page uid (e.g. "123") or link configuration (e.g. "t3://page?uid=123":

::

    mod.web_layout.tt_content_footer.showSlugFor = header_link, other_field


Configuration of conditions
===========================

For specific fields, it is possible to define conditions.

It is possible to not show specific values for specific fields:

::

    mod.web_layout.tt_content_footer.displayCond {
        frame_class {
            10 {
                valuesNot = none, default, ruler-after
            }
        }
    }

It is possible to not show specific fields if the content element is not visible:

::

    mod.web_layout.tt_content_footer.displayCond {
        frame_class {
            10 {
                hideIfNotVisible = 1
            }
        }
    }

It is possible to show specific fields only for specific content elements, e.g. the subheader field only for image content elements:

::

    mod.web_layout.tt_content_footer.displayCond {
        subheader {
            10 {
                field = CType
                values = image,textpic,textmedia
            }
        }
    }


Change labels
=============

To change the labels which are shown in the content element footer, you can overwrite the TCA setting...

::

    $GLOBALS['TCA']['tt_content']['columns']['subheader']['label'] = 'LLL:EXT:your_ext/Resources/Private/Language/locallang.xlf:subheader';

...or overwrite the language file:

::

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:frontend/Resources/Private/Language/locallang_ttc.xlf'][] = 'EXT:your_ext/Resources/Private/Language/Overrides/frontend-locallang_ttc.xlf';


--------------
Known problems
--------------

Has problems with EXT:rte_ckeditor_image because both extensions provide a BE preview renderer and therefore overwrite $GLOBALS['TCA']['tt_content']['types']['text']['previewRenderer'].

That's why this extension has this configuration:

::

      "suggest": {
        "netresearch/rte-ckeditor-image": "Just for loading order"
      },

As a result, EXT:cefooter is loaded after EXT:rte_ckeditor_image and preview is provided by EXT:cefooter.


----
Todo
----

- Find a better solution for configuration of display conditions.


---------
ChangeLog
---------

See file **ChangeLog** in the extension directory.
